import moment from "moment";
import { extent, group, rollup } from "d3-array";

import { CalendarData as CD, simplifyLargeNumber } from "@lgv/visualization-chart";

/**
 * CalendarData is a data abstraction for calendar structures.
 * @param {array} data - object with key/value pairs of start/end.
 * @param {object} params - key/values to enrich data during condition
 */
class CalendarData extends CD {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

        // update self
        this.cellExtent = extent(Array.from(rollup(this.enrich(data), v => v.length, d => d.week, d => d.day)).map(w => Array.from(w[1]).map(d => d[1])).flat());

    }

    /**
     * Generate week indicies.
     */
    get weeks() {
        return moment().weeksInYear();
    }

    /**
     * Condition data for visualization requirements.
     * @param {array} data - object with key/value pairs of start/end.
     * @param {object} params - key/values to enrich data during condition
     * @returns An object or array of data specifically formatted to the visualization type.
     */
    condition(data, params) {
        return group(this.enrich(data), d => d.week, d => d.day);
    }

    /**
     * Generate day event detail object.
     * @param {d} object - d3.js data object
     * @param {e} Event - JavaScript Event object
     * @returns An object of key/value metadata pairs.
     */
    configureEventDetails(d,e) {
        return {
            events: d,
            label: this.extractLabel(d),
            value: this.extractValue(d),
            valueLabel: this.extractValueAsLabel(d),
            xy: [e.clientX, e.clientY]
        };
    }

    /**
     * Enrich data so that date spans are reflected as discrete datum.
     * @param {array} data - object with key/value pairs of start/end.
     */
    enrich(data) {
        return Array.from(this.fillDates(data), d => d[1])
            .flat()
            .map((d,i) => ({...d,...{ id: i, day: moment(d.date).isoWeekday(), week: moment(d.date).isoWeek()}}));
    }

    /**
     * Get label from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum label.
     */
    extractLabel(d) {
        return `w${d[0].week} ${this.weekdays[d[0].day-1]}`;
    }

    /**
     * Get value from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum value.
     */
    extractValue(d) {
        return d.length;
    }

    /**
     * Get value from data formatted as a label, i.e. mainly simplified for small spaces.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum value in more narrative form.
     */
    extractValueAsLabel(d) {
        return simplifyLargeNumber(d.length);
    }


}

export { CalendarData };
export default CalendarData;