import { scaleQuantize } from "d3-scale";
import { select } from "d3-selection";

import { ChartLabel, DomGrid, ActivityLegend, simplifyLargeNumber } from "@lgv/visualization-chart";

import { CalendarData as CD } from "../structure/calendar.js";
import { configuration, configurationColor } from "../configuration.js";
import style from "./style.css";

/**
 * ActivityCalendar is a time series visualization.
 * @param {CalendarData} class - JavaScript class with calendar helpers
 * @param {array} data - objects where each represents a path in the hierarchy
 * @param {string} date - iso 8601 date value
 * @param {node} dom - HTML dom node
 * @param {string} label - unique prefix for common element ids/css selectors
 * @param {string} name - label for top-level container
 * @param {string} timezone - moment.hs timezone value
 */
class ActivityCalendar extends DomGrid {
    constructor(data, CalendarData=null, timezone="en-us", label=configuration.branding, name=configuration.name) {

        // initialize inheritance
        super(data, CalendarData ? CalendarData : new CD(data,{timezone:timezone}), label, name);

        // update self
        this.cell = null;
        this.cellSwatch = null;
        this.classCell = `${label}-cell`;
        this.classCellSwatch = `${label}-cell-swatch`;
        this.classDay = `${label}-day`;
        this.classDow = `${label}-dow`;
        this.classGrid = `${label}-grid`;
        this.classWeek = `${label}-week`;
        this.classWoy = `${label}-woy`;
        this.day = null;
        this.dow = null;
        this.grid = null;
        this.Legend = new ActivityLegend(this.legend,label,name);
        this.week = null;
        this.weekCount = this.Data.weeks;
        this.woy = null;

    }

    /**
     * Extract cell height.
     */
    get cellHeight() {
        return this.cell.node().offsetWidth;
    }

    /**
     * Generate calendar grid iso 8601 date values.
     * @returns A list of objects of key/value pairs of day, week.
     */
    get cells() {
        return [...Array(this.Data.weekdays.length).keys()].map(d => { 
            return [...Array(this.Data.weeks).keys()].map(w => ({
                day: d + 1,
                week: w + 1
            }))
        }).flat();
    }

    /**
     * Generate cell swatch value.
     * @returns A string representing a CSS color value.
     */
    get color() {
        return scaleQuantize()
            .domain([0,this.Data.cellExtent[1]])
            .range(["transparent"].concat(configurationColor));
    }

    /**
     * Generate legend data.
     * @returns A list of objects with key/value pairs of label,swatch,value
     */
    get legend() {
        return [0].concat(this.color.thresholds()).map((d,i) => ({label:simplifyLargeNumber(d),swatch:this.color.range()[i],value:d}));
    }

    /**
     * Declare cell click event.
     */
    configureCellEventClick() {
        this.cellSwatch
            .on("click", (e,d) => this.configureEvent("cell-click",d,e));
    }

    /**
     * Declare cell mouseput events.
     */
    configureCellEventMouseout() {
        this.cellSwatch
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classCellSwatch);

                // send cell to parent
                this.artboard.dispatch("cell-mouseout", { bubbles: true });

            });
    }

    /**
     * Declare cell mouseover event.
     */
    configureCellEventMouseover() {
        this.cellSwatch
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classCellSwatch} active`);

                // send cell to parent
                this.configureEvent("cell-mouseover",d,e);

            });
    }

    /**
     * Declare cell mouse events.
     */
    configureCellEvents() {
        this.configureCellEventClick();
        this.configureCellEventMouseout();
        this.configureCellEventMouseover();
    }

    /**
     * Position and minimally style cell in dom element.
     */
    configureCell() {
        this.cell
            .attr("class", d => `${this.classCell} ${d.day == this.Data.weekdays.count ? `${this.classCell}-last-in-column` : ""}`)
            .attr("data-label", d => `${d.week}-${d.day}`)
            .style("height", d => `${this.cellHeight}px`)
            .style("pointer-events", "none");
    }

    /**
     * Position and minimally style cell swatch in dom element.
     */
    configureCellSwatch() {
        this.cellSwatch
            .attr("class", this.classCellSwatch)
            .style("background-color", d => this.color(this.Data.extractValue(d)))
            .style("pointer-events", "all");
    }

    /**
     * Position and minimally style containing group in SVG dom element.
     */
    configureContainer() {
        this.content
            .attr("class", this.classContainer)
            .style("display", "flex")
            .style("flex-wrap", "wrap")
            .style("width", "100%");
    }

    /**
     * Position and minimally style day date in dom element.
     */
    configureDay() {
        this.day
            .attr("class", this.classDay)
            .style("height", `calc(100% / ${this.Data.weekdays.length})`)
            .style("pointer-events", "all")
            .html(d => d[0]);
    }

    /**
     * Position and minimally style day of week group in dom element.
     */
    configureDow() {
        this.dow
            .attr("class", this.classDow);
    }

    /**
     * Position and minimally style day of week + cell group in dom element.
     */
    configureGrid() {
        this.grid
            .attr("class", this.classGrid);
    }

    /**
     * Position and minimally style week value in dom element.
     */
    configureWeek() {
        this.week
            .attr("class", this.classWeek)
            .style("pointer-events", "all")
            .html(d => d);
    }

    /**
     * Position and minimally style week of year group in dom element.
     */
    configureWoy() {
        this.woy
            .attr("class", this.classWoy);
    }

    /**
     * Generate cell rectangle in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateCell(selection) {
        return selection
            .selectAll(`.${this.classCell}`)
            .data(d => this.cells)
            .join(
                enter => enter.append("div"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate cell fill shape in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateCellSwatch(selection) {
        return selection
            .selectAll(`.${this.classCellSwatch}`)
            .data(d => {
                let weekValues = this.data.get(d.week);
                let dayValues = weekValues !== undefined ? weekValues.get(d.day) : null;
                return dayValues ? [dayValues] : [];
            })
            .join(
                enter => enter.append("span"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate visualization.
     */
    generateChart() {
        
        // initialize labeling
        this.Label = new ChartLabel(this.artboard, this.Data);

        // generate weeks of year
        this.week = this.generateWeek(this.woy);
        this.configureWeek();

        // generate days of week
        this.day = this.generateDay(this.dow);
        this.configureDay();

        // wrap for content to ensure render within artboard
        this.content = this.generateContainer(this.grid);
        this.configureContainer();

        // generate each cell
        this.cell = this.generateCell(this.content);
        this.configureCell();

        // generate cell swatch
        this.cellSwatch = this.generateCellSwatch(this.cell);
        this.configureCellSwatch();
        this.configureCellEvents();

    }

    /**
     * Generate artboard and container for visualization.
     */
    generateCore() {

        // generate dom artboard
        this.artboard = this.generateArtboard(this.container);
        this.configureArtboard();

        // generate group for week of year
        this.woy = this.generateWoy(this.artboard);
        this.configureWoy();

        // generate group for day of week and cells
        this.grid = this.generateGrid(this.artboard);
        this.configureGrid();

        // generate group for day of week
        this.dow = this.generateDow(this.grid);
        this.configureDow();

    }

    /**
     * Generate day in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateDay(selection) {
        return selection
            .selectAll(`.${this.classDay}`)
            .data(d => d)
            .join(
                enter => enter.append("span"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate day of week group in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateDow(selection) {
        return selection
            .selectAll(`.${this.classDow}`)
            .data([this.Data.weekdays])
            .join(
                enter => enter.append("div"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate day of week + cell group in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateGrid(selection) {
        return selection
            .selectAll(`.${this.classGrid}`)
            .data([1])
            .join(
                enter => enter.append("div"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate week in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateWeek(selection) {
        return selection
            .selectAll(`.${this.classWeek}`)
            .data(d => d)
            .join(
                enter => enter.append("span"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate week of year group in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateWoy(selection) {
        return selection
            .selectAll(`.${this.classWoy}`)
            .data([[...Array(this.weekCount).keys()].map(w => w+1)])
            .join(
                enter => enter.append("div"),
                update => update,
                exit => exit.remove()
            );
    }

};

export { ActivityCalendar };
export default ActivityCalendar;
