import { configuration as config, configurationLayout as configLayout } from "@lgv/visualization-chart";
import packagejson from "../package.json";

const configuration = {
    branding: config.branding,
    name: packagejson.name.split("/")[1]
};

const configurationColor = ["lightgrey","grey","black"];

export { configuration, configurationColor };
export default configuration;
