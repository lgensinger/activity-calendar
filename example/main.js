import moment from "moment";

import packageJson from "../package.json";
import { ActivityCalendar } from "../src/index.js";
import { processEvent, renderDefault } from "@lgv/visualization-chart";

let data = [...Array(200).keys()].map(i => { 
    let start = moment().subtract(Math.floor(Math.random() * 300),"day");
    return {
        time_start: start.utc().format(), 
        time_end: moment(start).add(Math.floor(Math.random() * 3),"day").utc().format(), 
        event_title: `Event ${i}`
    }
});

// get elements
let container = document.getElementsByTagName("figure")[0];
let sourceContainer = document.getElementsByTagName("code")[0];
let outputContainer = document.getElementsByTagName("code")[1];

/**
 * Render initial visualization.
 * @param {element} container - DOM element
 * @param {array} data - object with key/value pairs of path
 */
function startup(data,container) {

    // update version in header
    let h1 = document.querySelector("h1");
    let title = h1.innerText;
    h1.innerHTML = `${title} <span>v${packageJson.version}</span>`;

    // render source data
    renderDefault(data,sourceContainer,outputContainer);

    // initialize
    const ac = new ActivityCalendar(data);

    // render visualization
    ac.render(container);

    // render legend
    ac.Legend.render(document.querySelector("#legend"));

}

// load document
document.onload = startup(data,container);

// attach events
container.outputContainer = outputContainer;
container.addEventListener("cell-click",processEvent);
container.addEventListener("cell-mouseover", processEvent);
container.addEventListener("cell-mouseout", processEvent);